# TEVIS Import



## Beschreibung
Der TEVIS Import Flow importiert in einem regelmäßigen Intervall die Vorlaufzeiten für ausgewählte Anliegen an ausgewählten Standorten bzw. Kalendern. Diese werden in einer MS SQL Datenbank gespeichert, Beispielhaft kann hierzu die Datenbank des DUVA Servers genutzt werden. 

## Installation
Dieser Flow basiert auf Node Red. In bestehende Node Red Instanzen kann die [tevis-import.json] Datein über die Importfunktion aufgenommen werden. Anzupassen sind dann die Tevis_Base Parameter des Flows auf die entsprechende URL, unter der das TEVIS-Backend inkl. seiner API für die Node-Red-Instanz erreichbar ist.
In TEVIS selbst ist ein entsprechend berechtigter User für die API Abfrage einzurichten. Im Node `TEVIS / Terminverwaltung` sind die Logindaten des API Users zu hinterlegen. Ggf. muss hier auch der Proxy für den Zugriff auf die API angepasst werden.
Im Node `ID-Vorbereitung` sind die IDs der Anliegen und Kalender zu hinterlegen, deren Vorlaufzeiten abgefragt werden sollen. Diese sind aus der jeweiligen TEVIS Instanz zu beziehen und individuell. 
Zur Speicherung in einer MS SQL Datenbank ist auch das Create Statement beigefügt, welches eine Tabelle erzeugt, die durch das Insert Statement im Node `UDP` gefüllt wird. In diesem Node sind die jeweils eigenen Logindaten des MS SQL Servers zu ergänzen.

## Support
Sollten bei der Wiederverwendung Probleme auftreten, öffnen Sie bitte entsprechende Issues in diesem Repository.

## Roadmap
- Verbesserung der Konfigurierbarkeit
- Ergänzung weiterer Speicheroptionen (z.B. FROST Server, Postgres SQL) - Bei Bedarf gerne einbringen

## Contributing
Eine urbane Datenplattform erfordert die Zusammenarbeit zwischen Kommunen, um Entwicklungskapazitäten effizient einzusetzen und von den Erfolgen gemeinsam zu partizipieren. Daher sind alle Kommunen dazu eingeladen an den Flows mitzuentwickeln und eigene Flows für weitere Importe oder Dashboards einzubringen. Nur gemeinsam werden wir das Projekt skalieren können.

## Authors and acknowledgment
Stadt Oberhausen

## Project status
Active
